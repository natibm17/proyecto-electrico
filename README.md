# Algoritmo genético para optimizar un sistema de segmentación de audios de larga duración
## Natalia Bolaños Murillo
### El siguiente programa consiste en un algoritmo genético para la optimizar un sistema de segmentación de audios de larga duración.
### Se implementó con 3 archivos de Python:
#### *Segmentation.py* contiene una clase Segmentation con todos los métodos para la segmentación y obtención de datos.
#### *Genetics.py* contiene una clase Cromosome y una clase Population utilizadas para la implementación del algoritmo genético.
#### *Main.py* contiene la lógica del sistema general. 

#### Prerrequisitos:
##### -SoX
##### -python3
##### -python3-numpy
##### -python3-pandas


### Instalación y Ejecución:

#### Se cuenta con un Makefile, para utilizarlo se debe instalar el comando make:
``` sudo apt-get install make ```

#### Comandos para utilizar el Makefile:
##### Instalar dependencias:
``` make install_dep```
##### Ejecutar el programa:
``` make ga ```
##### Instalar dependencias y ejecutar el programa:
```make all```

#### Para ejecutar la aplcación sin el uso del Makefile:
``` python3 main.py```

#### El audio a segmentar debe ser tipo WAV y debe guardarse en el mismo directorio que los archivos de Python.

